#pragma once
#include "LinkedList.h"

/* a positive-integer value stack, with no size limit */
typedef struct stack
{
	node* head;
} stack;

void initStack(stack* s);
void cleanStack(stack* s);

void push(stack* s, unsigned int element);
int pop(stack* s); // Return -1 if stack is empty