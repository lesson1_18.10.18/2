#pragma once

typedef struct node
{
	int num;
	node* next;
} node;
