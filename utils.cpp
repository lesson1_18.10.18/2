#include "utils.h"
#include "stack.h"
#include <iostream>

using namespace std;

void reverse(int* nums, unsigned int size)
{
	int i = 0;
	stack* s = new stack;

	for (i = 0; i < size; i++)
	{
		push(s, nums[i]);
	}
	for (i = 0; i < size; i++)
	{
		nums[i] = pop(s);
	}
}

int* reverse10()
{
	int* nums = new int[10];
	int number = 0;
	int i = 0;

	std::cout << "Please enter 10 numbers:" << std::endl;

	for (i = 0; i < 10; i++)
	{
		std::cout << "number " << (i + 1) << ": ";
		std::cin >> number;
		nums[i] = number;
	}
	
	reverse(nums, 10);

	return nums;
}