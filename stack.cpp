#include "stack.h"

void initStack(stack* s)
{
	s->head = nullptr;
}

void cleanStack(stack* s)
{
	delete(s->head);
}

void push(stack* s, unsigned int element)
{
	node* temp = nullptr;

	temp = (s->head);
	s->head = new node;
	s->head->next = temp;
	s->head->num = element;
}

int pop(stack* s)
{
	int num;
	node* temp = nullptr;

	if (s->head == nullptr)
	{
		num = -1;
	}
	else
	{
		temp = s->head->next;
		num = s->head->num;
		s->head = temp;
	}

	return num;
}